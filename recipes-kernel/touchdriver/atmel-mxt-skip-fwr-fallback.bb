SUMMARY = "Workaround to skip the sysfs firmware fallback of the atmel maxTouch at boot up"
# The current version of the atmel_mxt_ts driver tries to load a controller configuration
# on each driver probe. However, if no config is provided or can't be provided because
# the driver is build in, the driver probing process is delayed by 60 seconds because
# of a sysfs firmware load fallback. To shorten this delay, this recipe adds a udev rule
# that cancels the sysfs firmware fallback.

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

FILESEXTRAPATHS:append := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://skip_atmel-mxt_fwr_fallback.rules \
"

do_install() {
    install -d ${D}${sysconfdir}/udev/rules.d/
    install -m 0644 ${WORKDIR}/skip_atmel-mxt_fwr_fallback.rules ${D}${sysconfdir}/udev/rules.d
}
