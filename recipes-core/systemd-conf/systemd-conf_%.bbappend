FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://70-eth.network \
    file://80-mlan.network \
"

FILES:${PN} += " \
    ${sysconfdir}/systemd/network/70-eth.network \
    ${sysconfdir}/systemd/network/80-mlan.network \
"

do_install:append() {
    install -d ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/70-eth.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/80-mlan.network ${D}${sysconfdir}/systemd/network
}