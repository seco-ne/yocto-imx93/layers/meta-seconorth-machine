require conf/machine/include/imx-base.inc
require conf/machine/include/arm/armv8-2a/tune-cortexa55.inc

MACHINE_FEATURES += "pci wifi bluetooth"
MACHINE_FEATURES:append:use-nxp-bsp = " optee jailhouse nxpiw612-sdio"

UBOOT_DTB_NAME = "imx93-11x11-evk"

IMX_DEFAULT_BOOTLOADER:use-nxp-bsp = "u-boot-seco-imx"
IMX_DEFAULT_BOOTLOADER:use-mainline-bsp = "u-boot-fslc"

MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS:append = " \
    linux-firmware-sd8997 \
"

# Is broken on kernel 6.6
# MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS:append = " \
#   packagegroup-seconorth-touchdriver \
# "

LOADADDR = ""
UBOOT_SUFFIX = "bin"
UBOOT_MAKE_TARGET = ""

SPL_BINARY = "spl/u-boot-spl.bin"

UBOOT_CONFIG ??= "sd"
UBOOT_CONFIG[sd]   = "${UBOOT_CONFIG_BASENAME}_defconfig,sdcard"
UBOOT_CONFIG[ecc]  = "${UBOOT_CONFIG_BASENAME}_inline_ecc_defconfig"
UBOOT_CONFIG[ld]   = "${UBOOT_CONFIG_BASENAME}_ld_defconfig"

# Set ATF platform name
ATF_PLATFORM = "imx93"

IMXBOOT_TARGETS = "flash_singleboot"

IMX_BOOT_SOC_TARGET = "iMX9"
IMX_BOOT_SEEK = "32"

# We have to disable SERIAL_CONSOLE due to auto-serial-console
SERIAL_CONSOLES = "115200;ttyLP0"

IMX_DEFAULT_BSP = "nxp"

# MACHINE_RELEASE_ARTEFACTS is used by the CI to package 
# the machine-specific files for the release
MACHINE_RELEASE_ARTEFACTS = " \
    imx-boot-seco-mx93-sd.bin-flash_singleboot \
    ${@' '.join([ dtb.split('/')[-1] for dtb in d.getVar('KERNEL_DEVICETREE').split() ] )} \
    "
