FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://sdiouart8997_combo_v4.bin"

do_install:append() {
  install -d ${D}${nonarch_base_libdir}/firmware/mrvl
  install -m 0644 ${WORKDIR}/sdiouart8997_combo_v4.bin ${D}${nonarch_base_libdir}/firmware/mrvl/sdiouart8997_combo_v4.bin
}

FILES:${PN}-sd8997 += " \
  ${nonarch_base_libdir}/firmware/mrvl/sdiouart8997_combo_v4.bin \
"
