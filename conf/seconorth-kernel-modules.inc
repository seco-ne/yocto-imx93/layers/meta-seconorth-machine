SUMMARY = "${PN} kernel module."
HOMEPAGE = "https://www.seco.com"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

inherit module

SRC_URI = " \
    file://src/ \
    file://Makefile \
"

S = "${WORKDIR}/src"

do_configure:prepend () {
    cp -f ${WORKDIR}/Makefile ${S}/Makefile
    sed -i "s|SVNVERSION|${PV}|g" ${S}/Makefile
    sed -i "s|@PACKAGENAME@|${PN}|g" ${S}/Makefile
    sourcefiles=$(for i in $( find ${S} -iname \*\.c -not -name ${PN}.c -and -not -name \*.mod.c -exec basename -s .c {} \; );do echo -n " $i.o "; done)
    sed -i "s|@PACKAGEOBJECTS@|${sourcefiles}|g" ${S}/Makefile
    sed -i "s|@INCLUDES@|${STAGING_INCDIR}|g" ${S}/Makefile
}

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.
RRECOMMENDS:${PN} = "kernel-module-${PN}"
KERNEL_MODULE_AUTOLOAD += "${PN}"

