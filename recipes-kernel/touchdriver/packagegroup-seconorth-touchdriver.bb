# Copyright (C) 2014 SECO Northern Europe GmbH
DESCRIPTION = "SECO Northern Europe package group - touch driver"

inherit packagegroup


UNUSED_DRIVERS = " \
	auo-pixcir \
	edt-ft5x06 \
	himax \
	pixcir \
	egalaxi2c \
	ili2118 \
	scx0500633 \
	sis-i2c \
"

RDEPENDS:${PN} = " \
	atmel-mxt \
	ilitek \
"

RDEPENDS:${PN}:append:mx6q  = " \
	eetii2c \ 
	ni-force-ts \
"

# This is not used for fng system, so use the override
# this is ugly as the override comes from the distro
# layer. But this hack is ugly anyway an may be
# replaced using the atmel driver instead of the mainline
# driver
RDEPENDS:${PN}:append:seconorth  = " \
	atmel-mxt-skip-fwr-fallback \
"
