
# There a different versions of this file, so we keep this one with the hash in the name on our own mirror.
SRC_URI = "${FSL_MIRROR}/${BP}-${IMX_SRCREV_ABBREV}-d858fcbb47482a898a1af5fe5f3f8be53bb21fac793b33e9bcdfd2b4dda79d3c.bin;fsl-eula=true"
SRC_URI[md5sum] = "1359bc7d378bddfe1d8479eba05b05ec"
SRC_URI[sha256sum] = "d858fcbb47482a898a1af5fe5f3f8be53bb21fac793b33e9bcdfd2b4dda79d3c"
